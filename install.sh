sudo apt install python-bluez
pip3 install bleak
sudo apt-get install python3-dev python3-rpi.gpio

cp orba.service /lib/systemd/system/orba.service
chmod 755 orba.py
chmod 644 /lib/systemd/system/orba.service
systemctl daemon-reload
systemctl enable orba.service
systemctl start orba.service

#to stop orba
#systemctl stop orba.service