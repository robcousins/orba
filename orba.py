#!/usr/bin/python3

import asyncio
import os
import sys
import time
from bleak import BleakScanner
import mido
import subprocess
import RPi.GPIO as GPIO # Import Raspberry Pi GPIO library
import logging
import signal
import threading

logging.basicConfig(filename="/var/log/orba.log",
                            filemode='a',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            level=logging.DEBUG)

logging.info("orba service started")

def get_device_output(device_name):
    devices=mido.get_output_names()
    for device in range(len(devices)):
        if devices[device].find('Midi Through') != -1:
            logging.info(devices[device]+' found')
            return devices[device]
    logging.info('device not found '+device_name)
    return 0

def get_device_input(device_name):
    devices=mido.get_input_names()
    for device in range(len(devices)):
        if devices[device].find('Artiphon Orba') != -1:
            logging.info(devices[device]+' found')
            return devices[device]
    logging.info('device not found '+device_name)
    return 0

async def connect_orba(wanted_name):
    logging.info('scan started')
    try:
        device = await BleakScanner.find_device_by_filter(
            lambda d, ad: d.name and d.name.lower() == wanted_name.lower()
        )
        logging.info('scan finished')
        logging.debug(device.address)
        logging.info('trusting device finished')
        os.system("bluetoothctl trust "+device.address)
        logging.info('connecting device finished')
        os.system("bluetoothctl connect "+device.address)
    except Exception as e:
        logging.error(e)
        logging.error('make sure device is powered on and try again')

def disconect_orba(wanted_name):
    if get_device_input(wanted_name):
        print('removing')

def find_orba():
    result = subprocess.run(["aplaymidi", "-l" ," | grep Orba"], stdout=subprocess.PIPE)
    devices=result.stdout.decode('utf-8')
    if "Orba" in devices:
        return 1
    else:
        return 0

##Midi note routing
def channel_change(channel):
    if channel == 9: 
        return 0
    if channel == 8: 
        return 1
    if channel == 15: 
        return 2
    if channel == 0: 
        return 3
    return 0

def note_on(channel,note,velocity,time):
    channel=channel_change(channel)
    logging.info('note_on')
    logging.info(channel)
    message_out=mido.Message('note_on', channel=channel, note=note, velocity=velocity, time=time)
    outport.send(message_out)

def note_off(channel,note,velocity,time):
    channel=channel_change(channel)
    logging.info('note_off')
    logging.info(channel)
    message_out=mido.Message('note_off', channel=channel, note=note, velocity=velocity, time=time)
    outport.send(message_out)

def pitchwheel(channel,pitch):
    channel=channel_change(channel)
    logging.info('pitchwheel')
    logging.info(channel)
    message_out=mido.Message('pitchwheel', channel=channel, pitch=pitch)
    outport.send(message_out)

def aftertouch(channel,value):
    channel=channel_change(channel)
    logging.info('aftertouch')
    logging.info(channel)
    message_out=mido.Message('aftertouch', channel=channel, value=value)
    outport.send(message_out)    


def start_midi(message):
    logging.info(message)
    if message.type == 'aftertouch': aftertouch(message.channel,message.value)
    if message.type == 'control_change' and message.control == 2: pitchwheel(message.channel,(message.value*48))
    if message.type == 'note_on': logging.info(message.channel)
    if message.type == 'note_on': note_on(channel=message.channel,note=message.note,velocity=message.velocity,time=message.time)
    if message.type == 'note_off': note_off(message.channel,note=message.note,velocity=message.velocity,time=message.time)

def open_inport():
    while True:
        
        if find_orba():
            #logging.debug('device already exists check if port exist')
            try:
                if port:
                    #logging.debug('port aleady defined check if port is already open')
                    if port.closed:
                        port.close()
                        logging.debug('reopening port')
                        port = mido.open_input(get_device_input('Artiphon Orba'),callback=start_midi)
            except NameError:
                logging.debug('port does not exist opening port')
                port = mido.open_input(get_device_input('Artiphon Orba'),callback=start_midi)
        else:
            #print('device not found')
            try:
                if port:
                    logging.debug('closing port')
                    port.close()
                    port.callback = None
                    outport.panic()
            except NameError:
                logging.debug('device does not exist yet')
    
            time.sleep(.5)
            
open_inport_tread = threading.Thread(name='open_inport', target=open_inport)
open_inport_tread.start()

def button_callback(channel):
    logging.info("Button was pushed!")
    if find_orba():
         logging.info('orba found')
        
  
    else:
         logging.info('orba not found going to try to connect')
         asyncio.run(connect_orba("Artiphon Orba"))
    #GPIO.output(16,GPIO.LOW)

##Gobal vars
outport = mido.open_output(get_device_output('Midi Through'))
GPIO.setwarnings(False) # Ignore warning for now
GPIO.setmode(GPIO.BOARD) # Use physical pin numbering
GPIO.setup(16, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # Set pin 16 to be an input pin and set initial value to be pulled low (off)
GPIO.add_event_detect(16,GPIO.RISING,callback=button_callback) # Setup event on pin 16 rising edge

def main():

    while True:
        
        time.sleep(0.5)
 
def readConfiguration(signalNumber, frame):
    logging.info ('(SIGHUP) reading configuration')
    return

def terminateProcess(signalNumber, frame):
    logging.info ('(SIGTERM) terminating the process')
    exit()

def receiveSignal(signalNumber, frame):
    logging.info('Received:', signalNumber)
    return

def exit():
    logging.info ('cleanning up')
    outport.panic()
    GPIO.cleanup() # Clean up
    sys.exit()
          
if __name__ == '__main__':
    signal.signal(signal.SIGHUP, readConfiguration)
    signal.signal(signal.SIGINT, terminateProcess)
    signal.signal(signal.SIGQUIT, receiveSignal)
    signal.signal(signal.SIGILL, receiveSignal)
    signal.signal(signal.SIGTRAP, receiveSignal)
    signal.signal(signal.SIGABRT, receiveSignal)
    signal.signal(signal.SIGBUS, receiveSignal)
    signal.signal(signal.SIGFPE, receiveSignal)
    #signal.signal(signal.SIGKILL, receiveSignal)
    signal.signal(signal.SIGUSR1, receiveSignal)
    signal.signal(signal.SIGSEGV, receiveSignal)
    signal.signal(signal.SIGUSR2, receiveSignal)
    signal.signal(signal.SIGPIPE, receiveSignal)
    signal.signal(signal.SIGALRM, receiveSignal)
    signal.signal(signal.SIGTERM, terminateProcess)
    main()
